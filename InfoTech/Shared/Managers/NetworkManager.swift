//
//  NetworkManager.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case requestFailed
    // Add more error cases as needed
}

class NetworkManager {
    private let session: URLSession
    private let queue = DispatchQueue.global()
    
    init() {
        let configuration = URLSessionConfiguration.default
        session = URLSession(configuration: configuration)
    }
    
    func makeRequest(urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        guard let url = URL(string: urlString) else {
            completion(.failure(NetworkError.invalidURL))
            return
        }
        
        queue.async {
            let task = self.session.dataTask(with: url) { data, _, error in
                if let error = error {
                    completion(.failure(error))
                } else if let data = data {
                    completion(.success(data))
                } else {
                    completion(.failure(NetworkError.requestFailed))
                }
            }
            task.resume()
        }
    }
}
