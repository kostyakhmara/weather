//
//  SearchTableView.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import CombineCocoa
import SnapKit
import Then
import UIKit

final class SearchTableView: UIView {
    // MARK: - Properties
    private enum Constants {
        static let standardOffset = CGFloat(16)
        static let buttonSize = CGFloat(44)
    }
    
    private let searchBar = UISearchBar().then {
        $0.searchBarStyle = .minimal
        $0.enablesReturnKeyAutomatically = false
        $0.placeholder = "Search"
    }

    private(set) var activityIndicator = UIActivityIndicatorView().then {
        $0.color = .black
    }


    // MARK: - Reactive Publishers
    private(set) lazy var searchTextPublisher = searchBar.searchTextField.textPublisher
        .debounce(for: 0.3, scheduler: DispatchQueue.main)
        .replaceNil(with: "")
        .map {
            $0.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        .removeDuplicates()
        .eraseToAnyPublisher()

    private(set) lazy var searchButtonTapped = searchBar.searchButtonClickedPublisher

    // MARK: - Initialization
    init() {
        super.init(frame: .zero)
        setupUI()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods
    private func setupUI() {
        backgroundColor = .white

        addSubview(searchBar)
        addSubview(activityIndicator)

        
        searchBar.snp.makeConstraints {
            $0.left.right.equalToSuperview().inset(Constants.standardOffset)
            $0.centerY.equalToSuperview()
        }

        activityIndicator.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.right.equalTo(searchBar.snp.right).offset(-Constants.buttonSize)
        }
    }
}
