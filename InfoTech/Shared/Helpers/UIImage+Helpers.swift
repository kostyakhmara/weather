//
//  UIImage+Helpers.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import Foundation
import UIKit

extension UIImage {
    static func systemImage(name: String, config: UIImage.SymbolConfiguration, color: UIColor = .red) -> UIImage? {
        UIImage(systemName: name)?
            .applyingSymbolConfiguration(config)?
            .withTintColor(color, renderingMode: .alwaysOriginal)
    }
}

extension UIImage.SymbolConfiguration {
    static let body = UIImage.SymbolConfiguration(font: .preferredFont(forTextStyle: .body))
}
