//
//  DetailCityVC.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import UIKit
import SnapKit
import Then
import MapKit
import Combine


class DetailCityVC: UIViewController{
    // MARK: - UI
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private(set) var activityIndicator = UIActivityIndicatorView().then {
        $0.style = .large
        $0.color = .black
    }
    
    // MARK: - Properties
    let viewModel: СitiesListVM
    let city: CityElement
    private var cancellables = Set<AnyCancellable>()
    let options: MKMapSnapshotter.Options = .init()
    
    // MARK: - Initialization
    init(viewModel: СitiesListVM, city: CityElement) {
        self.viewModel = viewModel
        self.city = city
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupUI()
        bindUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.weatherItem = []
    }
    
    // MARK: - Methods
    private func setupNavigationBar() {
        title = city.name
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        addImageToTableViewHeader()
        
        tableView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        activityIndicator.startAnimating()
    }
    
    private func bindUI() {
        let coord = city.coord
        viewModel.getWeather(lat: coord.lat, lon: coord.lon)
        
        viewModel.$weatherItem
            .filter { $0.isEmpty }
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] _ in
                tableView.delegate = self
                tableView.dataSource = self
                activityIndicator.stopAnimating()
            }
            .store(in: &cancellables)
    }
    
    private func addImageToTableViewHeader() {
        DispatchQueue.main.async {
            self.сreateImageFromMap { [unowned self] image in
                if let image = image {
                    let headerView = StretchyTableHeaderView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.frame.height * 0.3))
                    headerView.imageView.image = image
                    tableView.tableHeaderView = headerView
                }
            }
        }
    }
    
    private func сreateImageFromMap(completion: @escaping (UIImage?) -> Void) {
        let coord = city.coord
        
        options.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: coord.lat, longitude: coord.lon), span: MKCoordinateSpan(latitudeDelta: 0.4, longitudeDelta: 0.4))
        options.size = CGSize(width: self.view.bounds.width, height: view.frame.height * 0.3)
        options.mapType = .standard
        options.showsBuildings = true
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if let snapshot = snapshot {
                let image = snapshot.image
                completion(image)
            } else if let _ = error {
                completion(nil)
            }
        }
    }
    
    private func cellSetup(weather: WeatherItem) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "weatherCell")
        cell.selectionStyle = .none
        cell.textLabel?.text = weather.title
        cell.detailTextLabel?.text = weather.description
        return cell
    }
    
}

// MARK: - UITableViewDataSource
extension DetailCityVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.weatherItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cellSetup(weather: viewModel.weatherItem[indexPath.row])
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension DetailCityVC: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let headerView = self.tableView.tableHeaderView as! StretchyTableHeaderView
        headerView.scrollViewDidScroll(scrollView: scrollView)
    }
}
