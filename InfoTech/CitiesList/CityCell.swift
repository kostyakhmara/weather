//
//  CityCell.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import Foundation
import UIKit

final class CityCell: UITableViewCell {
    // MARK: - Constants
    private enum Constants {
        static let standardOffset = CGFloat(16)
        static let leftOffset = CGFloat(12)
        static let horizontalOffset = CGFloat(24)
        static let imageViewSize = CGFloat(100)
        static let orderInfoStackWidthRatio = CGFloat(0.2)
        static let middleSectionWidthRatio = CGFloat(0.567)
    }
    // MARK: - UI
    private let cityImage = UIImageView().then {
        $0.clipsToBounds = true
        $0.contentMode = .scaleAspectFit
    }
    private let cityLabel = UILabel()
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setupUI() {
        selectionStyle = .none
        contentView.addSubview(cityImage)
        cityImage.snp.makeConstraints {
            $0.left.equalToSuperview().offset(Constants.horizontalOffset)
            $0.centerY.equalToSuperview()
            $0.size.equalTo(Constants.imageViewSize)
        }
        
        contentView.addSubview(cityLabel)
        cityLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(cityImage.snp.right).offset(16)
        }
    }
    
    func setupCell(rowNumber: Int, cityName: String) {
        cityLabel.text = cityName
        var url: URL
        if rowNumber % 2 == 0 {
            url = URL(string: "https://infotech.gov.ua/storage/img/Temp3.png")!
        } else {
            url = URL(string: "https://infotech.gov.ua/storage/img/Temp1.png")!
        }
        cityImage.kf.setImage(with: url)
    }
}
