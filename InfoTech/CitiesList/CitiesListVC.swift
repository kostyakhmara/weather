//
//  CitiesListVC.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import UIKit
import SnapKit
import Then
import Combine
import CombineCocoa
import Kingfisher

class CitiesListVC: UIViewController {
    // MARK: - Constants
    private enum Constants {
        static let searchViewHeight = CGFloat(64)
        static let headerHeight = CGFloat(42)
    }
    
    // MARK: - UI
    private let contentView = UIView()
    private let tableView = UITableView(frame: .zero, style: .grouped).then {
        $0.rowHeight = 70
    }
    private let searchTableView = SearchTableView()
    private let separatorView = UIView().then {
        $0.backgroundColor = .opaqueSeparator
    }
    
    private(set) var activityIndicator = UIActivityIndicatorView().then {
        $0.style = .large
        $0.color = .black
    }
    
    // MARK: - Properties
    private let viewModel = СitiesListVM()
    private var cancellables = Set<AnyCancellable>()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavgitation()
        setupUI()
        bindUI()
    }
    
    // MARK: - Methods
    private func setupNavgitation() {
        title = "Сities"
    }
    
    private func setupUI() {
        view.backgroundColor = .white
        view.addSubview(searchTableView)
        searchTableView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(Constants.searchViewHeight)
        }
        
        view.addSubview(separatorView)
        separatorView.snp.makeConstraints {
            $0.top.equalTo(searchTableView.snp.bottom)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(0.5)
        }
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.top.equalTo(separatorView.snp.bottom)
            $0.left.right.equalToSuperview()
            $0.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        tableView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        activityIndicator.startAnimating()
    }
    private func bindUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CityCell.self, forCellReuseIdentifier: CityCell.idealReuseIdentifier)
        viewModel.$filteredCities
            .filter { !$0.isEmpty }
            .receive(on: DispatchQueue.main)
            .sink { [unowned self] _ in
                tableView.reloadData()
            }
            .store(in: &cancellables)
        
        viewModel.activityIndicator.loading
            .receive(on: DispatchQueue.main)
            .assign(to: \.isAnimatingExposed, on: activityIndicator)
            .store(in: &cancellables)
        
        
        searchTableView.searchTextPublisher
            .removeDuplicates()
            .assign(to: \.searchText, on: viewModel)
            .store(in: &cancellables)
        
    }

}

// MARK: - UITableViewDataSource
extension CitiesListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.filteredCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: CityCell.self, for: indexPath)
        cell.setupCell(rowNumber: indexPath.row, cityName: viewModel.filteredCities[indexPath.row].name)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CitiesListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailCityVC = DetailCityVC(viewModel: viewModel, city: viewModel.filteredCities[indexPath.row])
        navigationController?.pushViewController(detailCityVC, animated: true)
    }
}
