//
//  City.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import Foundation

// MARK: - City
struct CityElement: Codable {
    let id: Int
    let name, state, country: String
    let coord: Coord
}

// MARK: - Coord
struct Coord: Codable {
    let lon, lat: Double
}

typealias Cities = [CityElement]
