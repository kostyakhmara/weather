//
//  СitiesListVM.swift
//  InfoTech
//
//  Created by Konstantin Khmara on 18.06.2023.
//

import Foundation
import Combine

class СitiesListVM {
    // MARK: - Properties
    private let networkManager = NetworkManager()
    private let token = "e7239f857e051e1c7fb3f4ce4dc1ca8e"
    private let fileName = "city_list.json"
    private(set) var activityIndicator = CombineActivityIndicator()
    private(set) var serchActivityIndicator = CombineActivityIndicator()
    private var cancellables = Set<AnyCancellable>()
    @Published var cities = Cities()
    @Published var filteredCities = Cities()
    @Published var searchText = ""
    @Published var weatherItem = [WeatherItem]()
    
    // MARK: - Initialization
    init() {
        bind()
    }
    
    // MARK: - Private Methods
    private func bind() {
        readFileFromProjectFolder(fileName: fileName)
            .trackActivity(activityIndicator)
            .sink { [unowned self] fileContent in
                if let content = fileContent {
                    let jsonData = Data(content.utf8)
                    let decoder = JSONDecoder()
                    cities = try! decoder.decode(Cities.self, from: jsonData)
                } else {
                    print("File not found.")
                }
            }
            .store(in: &cancellables)
        
        
        $searchText
            .combineLatest($cities) { text, cities in
                text.isEmpty ? cities : cities.filter {
                    $0.name.localizedCaseInsensitiveContains(text)
                }
            }
            .trackActivity(serchActivityIndicator)
            .assign(to: \.filteredCities, on: self)
            .store(in: &cancellables)
    }
    
    private func readFileFromProjectFolder(fileName: String) -> AnyPublisher<String?, Never> {
        return Future<String?, Never> { promise in
            DispatchQueue.global().async {
                guard let bundlePath = Bundle.main.path(forResource: fileName, ofType: nil) else {
                    DispatchQueue.main.async {
                        promise(.success(nil))
                    }
                    return
                }
                do {
                    let fileData = try Data(contentsOf: URL(fileURLWithPath: bundlePath))
                    let fileString = String(data: fileData, encoding: .utf8)
                    DispatchQueue.main.async {
                        promise(.success(fileString))
                    }
                } catch {
                    print("Error reading file: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        promise(.success(nil))
                    }
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    private func makeEnum(weatherData: Data) {
        let weather = try! JSONDecoder().decode(WeatherResponse.self, from: weatherData)
        weatherItem = [
            .description(desc: weather.weather.first?.description ?? ""),
            .temperature(temp: weather.main.temp),
            .minMaxTemperature(minMax: [weather.main.tempMin, weather.main.tempMax]),
            .humidity(hum: weather.main.humidity),
            .windSpeed(speed: weather.wind.speed)]
    }
    
    
    
    // MARK: - Methods
    func getWeather(lat: Double, lon: Double) {
        networkManager.makeRequest(urlString: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=\(token)&units=metric") { [unowned self] result in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.makeEnum(weatherData: response)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
